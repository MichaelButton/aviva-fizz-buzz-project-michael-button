﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using Fizzbuzz.Models;
using FizzbuzzWebApiService.Models;

namespace Fizzbuzz.Controllers
{
    public class FizzbuzzController : Controller
    {      
        public ActionResult Index()
        {
            FizzbuzzModel model = new FizzbuzzModel();
            if (Session != null && Session["FizzbuzzModel"] != null)
                Session["FizzbuzzModel"] = null;
            ViewBag.UserName = System.Environment.UserName;
            return View(model);
        }

        [HttpPost]
        public ActionResult Results(FizzbuzzModel myFizzbuzzModel, bool generateResults = true)
        {
            if (Request != null &&
                Request.Form != null &&
                Session != null )
            { 
                if (Request.Form["previousButton"] != null)
                {
                    FizzbuzzModel myModel = (FizzbuzzModel)Session["FizzbuzzModel"];
                    if (myModel.CurrentPage > 1)
                        myModel.CurrentPage--;
                    Session["FizzbuzzModel"] = myModel;
                    myFizzbuzzModel = myModel;
                }
                else if (Request.Form["nextButton"] != null)
                {
                    FizzbuzzModel myModel = (FizzbuzzModel)Session["FizzbuzzModel"];
                    if (myModel.CurrentPage < myModel.TotalPages)
                        myModel.CurrentPage++;
                    Session["FizzbuzzModel"] = myModel;
                    myFizzbuzzModel = myModel;
                }
                else if (generateResults)
                {
                    myFizzbuzzModel.GenerateResults();
                    if (Session != null && Session["FizzbuzzModel"] != null)
                        Session["FizzbuzzModel"] = myFizzbuzzModel;

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:25693");
                        
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //var response = client.GetAsync("Api/User/3").Result;
                        var response = client.PostAsJsonAsync("Api/User", new User
                        {
                            UserName = System.Environment.UserName,
                            Value = myFizzbuzzModel.Count
                        }).Result;

                        UserDTO returnValue = response.Content.ReadAsAsync<UserDTO>().Result;


                        //var response = client.PostAsync("Api/User", "www");
                        //if (response..IsSuccessStatusCode)
                        //{
                        //    string responseString = response.Content.ReadAsStringAsync().Result;
                        //}
                    }
                }
            }

            return View(myFizzbuzzModel);
        }
    }
}