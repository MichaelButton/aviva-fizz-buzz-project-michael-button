﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Fizzbuzz.Models;

namespace Fizzbuzz.Models
{
    public class FizzbuzzModel
    {
        public int PageSize = 20;
        public int CurrentPage = 1;
        public int TotalPages = 1;

        [Range(1, 1000, ErrorMessage = "Value must be a whole number between 1 and 1000")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#}")]
        public int Count { get; set; }
        
        public List<FizzbuzzResultItem> Results { get; set; }

        public void GenerateResults()
        {
            CurrentPage = 1;
            TotalPages = 0;
            bool IsWednesday = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? true : false;
            if (Count > 0 && Count < 1001)
            {
                Results = new List<FizzbuzzResultItem>();
                for (int i = 1; i <= Count; i++)
                {
                    FizzbuzzResultItem newItem = new FizzbuzzResultItem();
                    newItem.Value = i;
                    /*
                     Where the number is divisible by 3, you should instead print ‘fizz’
                     Where the number is divisible by 5, you should instead print 'buzz'
                     Where the number is divisible by 3 AND 5, you should instead print ‘fizz buzz’
                     Change the logic so that if today is Wednesday, the words 'wizz' and ‘wuzz' are substituted for 'fizz' and 'buzz'
                    */

                    if (newItem.Value % 3 == 0)
                    {
                        newItem.IsDivisibleByThree = true;
                        newItem.IsDivisibleByThreeText = IsWednesday ? "wizz" : "fizz";
                    }

                    if (newItem.Value % 5 == 0)
                    {
                        newItem.IsDivisibleByFive = true;
                        newItem.IsDivisibleByFiveText = IsWednesday ? "wuzz" : "buzz";
                    }

                    Results.Add(newItem);
                }

                if (Results.Count > 0)
                {
                    TotalPages = Results.Count / this.PageSize;
                    if (Results.Count % this.PageSize != 0 && Results.Count > this.PageSize)
                        TotalPages += 1;
                    else if (Results.Count > 0 && Results.Count < this.PageSize)
                        TotalPages = 1;
                }
            }
        }
    }
}