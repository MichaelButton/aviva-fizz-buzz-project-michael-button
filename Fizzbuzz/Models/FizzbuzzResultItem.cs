﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fizzbuzz.Models
{
    public class FizzbuzzResultItem
    {
        public int Value { get; set; }
        public bool IsDivisibleByThree { get; set; }
        public bool IsDivisibleByFive { get; set; }
        public string IsDivisibleByThreeText { get; set; }
        public string IsDivisibleByFiveText { get; set; }
    }
}