﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FizzbuzzWebApiService.Models;

namespace FizzbuzzWebApiService.Controllers
{
    /// <summary>
    /// Facilitates storage and retrieval of user and fizzbuzz values
    /// </summary>
    public class UserController : ApiController
    {
        /// <summary>
        /// Gets a list of users from the server.
        /// </summary>
        public IQueryable<UserDTO> GetUsers()
        {
            List<UserDTO> newList = new List<UserDTO>();
            for(int i = 1; i < 3;i++)
            {
                UserDTO newUser = new UserDTO();
                newUser.Id = i;
                newUser.UserName = i.ToString();
                newList.Add(newUser);
            }

            return newList.AsQueryable();
        }

        /// <summary>
        /// Gets a list of previous fizzbuzz values for a particular user from the server.
        /// </summary>
        /// <param name="id">The identifier of the user</param>
        /// <returns>The user's name and previously used fizzbuzz values</returns>
        [ResponseType(typeof(UserDetailsDTO))]
        public async Task<IHttpActionResult> GetUser(int id)
        {
            var userDetails = new UserDetailsDTO();
            userDetails.Id = id;
            userDetails.UserName = id.ToString();
            userDetails.FizzbuzzValues = new List<int>();
            for (int i = 1; i < 10; i++)
            {
                userDetails.FizzbuzzValues.Add(i);
            }
            
            return Ok(userDetails);
        }


        
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Random rnd = new Random();
            int newId = rnd.Next();
            var dto = new UserDTO()
            {
                Id = newId,
                UserName = user.UserName
            };

            return CreatedAtRoute("DefaultApi", new { id = newId }, dto);
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="id">id of user</param>
        /// <param name="value">fizzbuzz value</param>
        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="id">id of user</param>
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
