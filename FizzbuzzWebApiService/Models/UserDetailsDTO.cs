﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzbuzzWebApiService.Models
{
    public class UserDetailsDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public List<int> FizzbuzzValues { get; set; }
    }
}