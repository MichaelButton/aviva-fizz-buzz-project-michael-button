﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzbuzzWebApiService.Models
{
    /// <summary>
    /// Class to relate a user to their fizzbuzz values
    /// </summary>
    public class User
    {
        /// <summary>
        /// Represents storage identifier of user
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of user
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Associated value
        /// </summary>
        public int Value { get; set; }
    }
}