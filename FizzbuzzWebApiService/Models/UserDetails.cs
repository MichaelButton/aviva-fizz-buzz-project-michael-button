﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzbuzzWebApiService.Models
{
    /// <summary>
    /// Association of a user and their fizzbuzz values
    /// </summary>
    public class UserDetails
    {
        /// <summary>
        /// Represents storage identifier of user
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Associated fizzbuzz value
        /// </summary>
        public List<int> FizzbuzzValues { get; set; }
    }
}