﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fizzbuzz.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Fizzbuzz.Models;

namespace Fizzbuzz.Controllers.Tests
{
    [TestClass()]
    public class FizzbuzzControllerTests
    {
        [TestMethod()]
        public void IndexTest()
        {
            // Arrange
            FizzbuzzController controller = new FizzbuzzController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void ResultsTest()
        {
            // Arrange
            FizzbuzzController controller = new FizzbuzzController();

            // Act
            FizzbuzzModel myFizzbuzzModel = new FizzbuzzModel();
            ViewResult result = controller.Results(myFizzbuzzModel,true) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}